module gitlab.com/ChTBoner/gaudiobook_sorter

go 1.18

require (
	github.com/akamensky/argparse v1.4.0
	gopkg.in/vansante/go-ffprobe.v2 v2.1.1
)
