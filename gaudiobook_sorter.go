package main

import (
	"context"
	"fmt"
	"github.com/akamensky/argparse"
	"gopkg.in/vansante/go-ffprobe.v2"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"time"
)

func dir_check(paths [2]string) {
	for _, path := range paths {
		if _, err := os.Stat(path); os.IsNotExist(err) {
			fmt.Printf("Path %s does not exist\n", path)
			os.Exit(2)
		}
	}
}

func Contains(sl []string, name string) bool {
	for _, value := range sl {
		if value == name {
			return true
		}
	}
	return false
}

func ffprober(path string) (string, string) {
	ctx, cancelFn := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelFn()

	fmt.Println(path)

	fileReader, err := os.Open(path)
	if err != nil {
		log.Panicf("Error opening test file: %v", err)
		os.Exit(4)
	}

	data, err := ffprobe.ProbeReader(ctx, fileReader)
	if err != nil {
		log.Panicf("Error getting data: %v", err)
		os.Exit(5)
	}
	output := data.Format.TagList
	fmt.Println(output)

	return output["artist"].(string), output["title"].(string)
	// return "", ""

}

func createDirs(path string) {
	if err := os.MkdirAll(path, 0755); err != nil {
		log.Fatal(err)
	}
}

func main() {

	audiobook_exts := []string{".m4b", ".m4a", ".mp3"}

	parser := argparse.NewParser("gaudiobook_sorter", "Sorts Audiobook by Author and Books, each in its own folder")
	source := parser.String("s", "source", &argparse.Options{Required: true, Help: "Location of unsorted audiobooks"})
	dest := parser.String("d", "dest", &argparse.Options{Required: true, Help: "destination of sorted audiobooks"})

	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
	}

	dir_check([2]string{*source, *dest})

	err = filepath.WalkDir(*source, func(path string, dir fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if !dir.IsDir() {
			if Contains(audiobook_exts, filepath.Ext(path)) {
				// path is an audiobook, let's sort it!
				// ffprober(path)
				author, title := ffprober(path)
				authorDir := filepath.Join(*dest, author)
				titleDir := filepath.Join(authorDir, title)
				destFilePath := filepath.Join(titleDir, filepath.Base(path))
				// fmt.Println(dirToCreatePath)
				createDirs(authorDir)
				createDirs(titleDir)
				os.Rename(path, destFilePath)
				// fmt.Println(destFilePath)
			}
		}
		return nil
	})
	if err != nil {
		fmt.Printf("error walking the path %q: %v\n", *source, err)
		return
	}
}
